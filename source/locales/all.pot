msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"

#: ../monophony/frontend/pages/search_page.py:23
#: ../monophony/frontend/pages/search_page.py:89
msgid "No results"
msgstr ""

#: ../monophony/frontend/pages/search_page.py:101
msgid "Songs"
msgstr ""

#: ../monophony/frontend/pages/search_page.py:102
#: ../monophony/frontend/pages/search_page.py:111
#: ../monophony/frontend/pages/search_page.py:120
#: ../monophony/frontend/pages/search_page.py:129
msgid "More"
msgstr ""

#: ../monophony/frontend/pages/search_page.py:110
msgid "Albums"
msgstr ""

#: ../monophony/frontend/pages/search_page.py:119
msgid "Community playlists"
msgstr ""

#: ../monophony/frontend/pages/search_page.py:128
msgid "Videos"
msgstr ""

#: ../monophony/frontend/pages/library_page.py:26
msgid "No playlists found"
msgstr ""

#: ../monophony/frontend/pages/library_page.py:29
msgid "Play all"
msgstr ""

#: ../monophony/frontend/pages/library_page.py:32
msgid "Playlists"
msgstr ""

#: ../monophony/frontend/windows/delete_window.py:15
msgid "Delete playlist?"
msgstr ""

#: ../monophony/frontend/windows/delete_window.py:16
#: ../monophony/frontend/windows/rename_window.py:18
msgid "Cancel"
msgstr ""

#: ../monophony/frontend/windows/delete_window.py:17
#: ../monophony/frontend/widgets/group_row.py:24
msgid "Delete"
msgstr ""

#: ../monophony/frontend/windows/main_window.py:29
msgid "About"
msgstr ""

#: ../monophony/frontend/windows/main_window.py:119
msgid "translator-credits"
msgstr ""

#: ../monophony/frontend/windows/main_window.py:120
msgid "Patrons"
msgstr ""

#: ../monophony/frontend/windows/main_window.py:122
msgid "Donate"
msgstr ""

#: ../monophony/frontend/windows/rename_window.py:15
msgid "Enter name..."
msgstr ""

#: ../monophony/frontend/windows/rename_window.py:19
#: ../monophony/frontend/windows/message_window.py:13
msgid "Ok"
msgstr ""

#: ../monophony/frontend/widgets/player.py:62
msgid "Remove from queue"
msgstr ""

#: ../monophony/frontend/widgets/player.py:65
msgid "Volume"
msgstr ""

#: ../monophony/frontend/widgets/player.py:81
msgid "Radio mode"
msgstr ""

#: ../monophony/frontend/widgets/song_row.py:21
msgid "Play"
msgstr ""

#: ../monophony/frontend/widgets/song_popover.py:37
msgid "Remove from downloads"
msgstr ""

#: ../monophony/frontend/widgets/song_popover.py:39
msgid "Download to Music folder"
msgstr ""

#: ../monophony/frontend/widgets/song_popover.py:51
msgid "Add to queue"
msgstr ""

#: ../monophony/frontend/widgets/song_popover.py:55
msgid "New playlist..."
msgstr ""

#: ../monophony/frontend/widgets/group_row.py:27
msgid "Rename..."
msgstr ""

#: ../monophony/frontend/widgets/group_row.py:35
msgid "Save to library"
msgstr ""

#: ../monophony/frontend/widgets/group_row.py:82
msgid "Could not rename"
msgstr ""

#: ../monophony/frontend/widgets/group_row.py:83
msgid "Playlist already exists"
msgstr ""
