Monophony is a free and open source Linux app for streaming music from YouTube. It has no ads and does not require an account.

Copyright © 2022-2023 zehkira, [MIT License](https://gitlab.com/zehkira/monophony/-/blob/master/source/LICENSE).

| [Install](https://flathub.org/apps/details/io.gitlab.zehkira.Monophony) | [Donate](https://ko-fi.com/zehkira) |
|-|-|

<img src='https://gitlab.com/zehkira/monophony/-/raw/master/assets/screenshot1.png' alt='screenshot'>
